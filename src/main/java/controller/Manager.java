package controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import model.Checkout;
import model.Client;
import view.Gui;

public class Manager implements Runnable {
	private int timer;
	private BlockingQueue<Checkout> checkoutList;
	private BlockingQueue<Client> clientList;
	private BufferedWriter writer;
	private Gui gui;
	public Thread t[];
	public  Manager() {
		
		timer= 0;
		checkoutList = new ArrayBlockingQueue<Checkout>(20);
		clientList = new ArrayBlockingQueue<Client>(20);
		try {
			writer = new BufferedWriter(new FileWriter("log.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		t = new Thread[8];
		
		for(int i=0; i< 8; i++) {
			Checkout c = new Checkout(i);
			checkoutList.add(c);
			t[i]= new Thread(c);
		}
		

		gui = new Gui();
	}
	
	public  synchronized void createClients(int minArrivalTime, int maxArrivalTime, int minProcessingTime, int maxProcessingTime ) {
		System.out.println(minArrivalTime +";" +maxArrivalTime + ";" +minProcessingTime + ";" +maxProcessingTime);
		int j=0;
			for(int i=0; i< 20; i++) {
				Client c = new Client(i, minArrivalTime, maxArrivalTime, minProcessingTime, maxProcessingTime);
				clientList.add(c);
				try {
					writer.write(" - " + c.getId()+ " ; " + c.getArrivalTime() 
							+ " ; " + c.getProcessingTime() +"\n");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(" - " + c.getId()+ " ; " + c.getArrivalTime() 
						+ " ; " + c.getProcessingTime());
			}
			
		
		
	}
	
	public synchronized void count() throws InterruptedException {
		for(int i=0; i<gui.getSimulationTime(); i++) {
			System.out.println(timer);
			for( Client c : clientList) {
				if(timer == c.getArrivalTime()) {
					minWaitingTimeCheckout(checkoutList).addClient(c);
				}
			}
			String timerText = timer+"";
			List<String> s = new ArrayList<String>();
			int j = 0;
	        for(Checkout c1: checkoutList){
				if(j < gui.getNrCheckouts()) {
					s.add(c1.toString());
				}
				
			}
			gui.updateGui(timerText, s);
			timer++;		

			
			
			try {
				wait(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public synchronized Checkout minWaitingTimeCheckout(BlockingQueue<Checkout> checkoutsList) throws InterruptedException { //minimum waiting time between existent checkouts
		for(Checkout c1: checkoutList){
			System.out.println("ID:" + c1.getId());
		}
		
		int min = checkoutsList.element().getWaitingTime();
		int cid = checkoutsList.element().getId();
		int j = 0;
        for(Checkout c1: checkoutList){
            if(j < gui.getNrCheckouts()){
                if(c1.getWaitingTime() < min) {
                    min = c1.getWaitingTime();
                    cid = c1.getId();
                }
            }
            j++;
        
		}
        System.out.println(" ---->"+ cid);
        j = 0;
        System.out.println("---"+ gui.getNrCheckouts());
        for(Checkout c1: checkoutList){
        	 if(j == cid)
                 return c1;
             else j++;
        }
		return null;
	}
	
	
	public void run() {
		// TODO Auto-generated method stub
		try {
			count();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public Gui getGui() {
		return gui;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void main(String args[]) {
		Manager m= new Manager();
		
		Thread tm = new Thread(m);
		System.out.println(m.getGui().getSimulationStarted()+"");
		while(!m.getGui().getSimulationStarted()) {
			//System.out.println(m.getGui().getSimulationStarted()+"");
			//System.out.println("true");
		}
		System.out.println(m.getGui().getMinArrivalTime() +";" +m.getGui().getMaxArrivalTime() + ";" +m.getGui().getMinProcessingTime() + ";" +m.getGui().getMaxProcessingTime());

		m.createClients(m.getGui().getMinArrivalTime(), m.getGui().getMaxArrivalTime(),  m.getGui().getMinProcessingTime(), m.getGui().getMaxProcessingTime());
		for(int i=0; i< m.getGui().getNrCheckouts(); i++) {
			m.t[i].start();
		}
		
		tm.start();
		for(int i=0; i< m.getGui().getNrCheckouts(); i++) {
			try {
				m.t[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			tm.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}


