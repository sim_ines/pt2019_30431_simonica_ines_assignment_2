package model;

import java.util.ArrayList;
import java.util.List;

public class Checkout implements Runnable{
	private int id;
	private int averageWaitingTime;
	private List<Client> clientsList;
	
	public Checkout(int id) {
		this.id = id;		
		this.clientsList = new ArrayList<Client>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAverageWaitingTime() {
		return averageWaitingTime;
	}

	public void setAverageWaitingTime(int averageWaitingTime) {
		this.averageWaitingTime = averageWaitingTime;
	}
	private synchronized void processClient() {
		System.out.println("orice");
		while(true) {
			if(!clientsList.isEmpty()) {
				if(clientsList.get(0).getProcessingTime() > 0) {
					System.out.println("checkout: " + id + " - " + clientsList.get(0).getId()+ " ; " + clientsList.get(0).getArrivalTime() 
							+ " ; " + clientsList.get(0).getProcessingTime());
					clientsList.get(0).setProcessingTime(clientsList.get(0).getProcessingTime()-1);
	 

				}
				else {
					clientsList.remove(0);

				}
			}
			try {
				wait(1000);
	
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public int getWaitingTime() { // total waiting time in a queue
		int totalProcessingTime=0;
		for(Client i : clientsList) {
			
			totalProcessingTime += i.getProcessingTime();
		}
		return totalProcessingTime;		
	}
	
	public void run() {
		processClient();
	}

	public List<Client> getClientsList() {
		return clientsList;
	}

	public void setClientsList(List<Client> clientsList) {
		this.clientsList = clientsList;
	}
	
	public void addClient(Client c) {
		this.getClientsList().add(c);
	}
	
	public String toString() {
		String s = "";
		for(Client c : clientsList) {
			s+= c.toString() + " ";
		}
		return s;
	}
}
