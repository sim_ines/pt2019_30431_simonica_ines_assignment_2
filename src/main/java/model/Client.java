package model;

import java.util.Random;

public class Client {
	private int id;
	private int arrivalTime;
	private int processingTime;
	
	public Client(int id, int arrivalTime, int processingTime) {
		this.id = id;
		this.arrivalTime= arrivalTime;
		this.processingTime =processingTime;
	}
	
	public Client(int id, int minArrivalTime, int maxArrivalTime, int minProcessingTime, int maxProcessingTime ) { //random client generator
		this.id = id;
		int aTime = new Random().nextInt(maxArrivalTime-minArrivalTime) + minArrivalTime;
		int pTime = new Random().nextInt(maxProcessingTime - minProcessingTime) + minProcessingTime;
		this.arrivalTime = aTime;
		this.processingTime = pTime;
	}
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}
	
	
	
	
	public synchronized void countDown() {
		while(this.processingTime>0) {
			this.processingTime--;		
			System.out.println(this.processingTime);
			try {
				wait(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	public String toString() {
		String s = "(ID: ";
		s += this.id + "; arrival: " +this.arrivalTime + "; process: " + this.processingTime + ")";
		return s;
	}
	
}
