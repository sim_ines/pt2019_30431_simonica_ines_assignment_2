package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Manager;

public class Gui {
	volatile private boolean simulationStarted;
	private JButton startSimulation;
	private JLabel timer;
	private JTextField checkoutInput;
	private JLabel nrCLabel;
	private JLabel nrC;
	
	private JLabel minALabel;
	private JTextField minAInput;
	
	private JLabel maxALabel;
	private JTextField maxAInput;
	
	private JLabel minPLabel;
	private JTextField minPInput;
	
	private JLabel maxPLabel;
	private JTextField maxPInput;
	
	private JLabel finalOutputLabel;
	private JTextField finalOutput;
	
	private List<JLabel> labelList;
	
	volatile private int minArrivalTime;
	volatile private int maxArrivalTime;
	volatile private int minProcessingTime;
	volatile private int maxProcessingTime;
	
	 private JLabel simulationTimeLabel;
	 private JTextField simulationTimeInput;
	
	volatile private int simulationTime;
	
	volatile private int nrOfCheckouts;
	
	public Gui() {
		simulationStarted= false;
		nrOfCheckouts= 1;
		labelList = new ArrayList<JLabel>();	
		
		startSimulation = new JButton("START");
		startSimulation.setVisible(true);
		
		nrCLabel = new JLabel("Checkout Nr: ");
		nrCLabel.setVisible(true);
			
		nrC = new JLabel("Nr. Checkouts: ");
		nrC.setVisible(true);
		
		checkoutInput = new JTextField("");
		checkoutInput.setVisible(true);
		checkoutInput.setEditable(true);
		
		
		minALabel = new JLabel("Min Arrival Time: ");
		minALabel.setVisible(true);
		
		minAInput = new JTextField("");
		minAInput.setVisible(true);
		minAInput.setEditable(true);
		
		
		maxALabel = new JLabel("Max Arrival Time: ");
		maxALabel.setVisible(true);
		
		maxAInput = new JTextField("");
		maxAInput.setVisible(true);
		maxAInput.setEditable(true);
		
		minPLabel = new JLabel("Min processing Time: ");
		minPLabel.setVisible(true);
		
		minPInput = new JTextField("");
		minPInput.setVisible(true);
		minPInput.setEditable(true);
		
		maxPLabel = new JLabel("Max processing Time: ");
		maxPLabel.setVisible(true);

		maxPInput = new JTextField("");
		maxPInput.setVisible(true);
		maxPInput.setEditable(true);
		
		simulationTimeLabel = new JLabel("Simulation Time: ");
		simulationTimeLabel.setVisible(true);
		
		
		finalOutputLabel = new JLabel("Final Output: ");
		finalOutputLabel.setVisible(true);
		
		finalOutput = new JTextField(""); //meeep
		finalOutput.setVisible(true);
		finalOutput.setEditable(false);
		
		simulationTimeInput = new JTextField("");
		simulationTimeInput.setVisible(true);
		simulationTimeInput.setEditable(true);
		
		
		
		timer = new JLabel("0");
		timer.setVisible(true);
			
		
		JFrame window = new JFrame();
	    JPanel panel = new JPanel();
	    panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
	
	      
	    panel.setVisible(true);
	    panel.add(startSimulation);
	    panel.add(nrCLabel);
	    panel.add(nrC);
	    panel.add(checkoutInput);
	    
	    panel.add(minALabel);
	    panel.add(minAInput);
	    
	    panel.add(maxALabel);
	    panel.add(maxAInput);
	    
	    panel.add(minPLabel);
	    panel.add(minPInput);
	    
	    panel.add(maxPLabel);
	    panel.add(maxPInput);
	   
	   
	    
	    panel.add(simulationTimeLabel);
	    panel.add(simulationTimeInput);
	        
	    
	    panel.add(timer);
	    
	    panel.add(finalOutputLabel);
	    panel.add(finalOutput);
	    
	    createLabels(8);

	    for(JLabel l: labelList) {
	    	panel.add(l);
	    }
	    
	    
	    window.setBounds(0,0,800,800);
	      
	    window.add(panel);
        
        
		startSimulation.addActionListener(new ActionListener() {
			
			public synchronized void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					simulationStarted = true;
					nrOfCheckouts = Integer.parseInt(checkoutInput.getText());
					minArrivalTime = Integer.parseInt(minAInput.getText());
					maxArrivalTime = Integer.parseInt(maxAInput.getText());
					minProcessingTime = Integer.parseInt(minPInput.getText());
					maxProcessingTime = Integer.parseInt(maxPInput.getText());
					simulationTime= Integer.parseInt(simulationTimeInput.getText());
					//System.out.println(nrOfCheckouts+"");System.out.println(simulationStarted + "");
			}
		});
        window.setVisible(true);


	}
	
	private void createLabels(int n) {
		for(int i=0; i<n; i++) {
			System.out.println(i+"");
			JLabel checkout = new JLabel("Checkout " + (i+1));
			checkout.setVisible(true);
			JLabel clientQ = new JLabel("closed");
			clientQ.setVisible(true);
			labelList.add(checkout);
			labelList.add(clientQ);
		}
	}
	
	public void updateGui(String timerText , List<String> l) {
		timer.setText(timerText);
		for(int i=0; i< l.size(); i++) {
			labelList.get(2*i+1).setText(l.get(i));
		}
	}

	public boolean getSimulationStarted() {
		return simulationStarted;
	}
	
	public int getNrCheckouts() {
		return nrOfCheckouts;
	}
	
	public int getMinArrivalTime() {
		return minArrivalTime;
	}
	
	public int getMaxArrivalTime() {
		return maxArrivalTime;
	}
	
	public int getMinProcessingTime() {
		return minProcessingTime;
	}
	
	public int getMaxProcessingTime() {
		return maxProcessingTime;
	}
	
	public int getSimulationTime() {
		return simulationTime;
	}
	
}
